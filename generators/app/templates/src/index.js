<% if (sass) { -%>
import './index.scss'
<% } else if (css) { -%>
import './index.css'
<% } %>
const prompt = 'Hello, World!'
<% if (html) { -%>
console.log(prompt)
<% } else { -%>
document.write('<h1>' + prompt + '</h1>')
<% } %>
