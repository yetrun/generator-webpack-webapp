# Simple WebPack Scaffold

## 使用

安装依赖

    yarn

开发（打开`http://localhost:8080`）

    yarn run dev

部署（生成到`dist`目录）

    yarn run build

<% if (static) { -%>
## 使用静态资源

将静态资源放在 `static` 目录下即可伺服。
<% }  -%>
