const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const CopyPlugin = require("copy-webpack-plugin")

module.exports = {
  mode: process.env.NODE_ENV || 'development',
  entry: './src/index.js',
  devServer: {
    contentBase: './dist',
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Simple WebPack Scaffold'
      <% if (html) { -%>,
        template: 'src/index.html'
          <% } -%>
    }),
    <% if (static) {-%>
    new CopyPlugin({
      patterns: [
        { from: 'static', to: '' }
      ]
    })
    <% } -%>
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' }
      <% if (css) { -%>,
        { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] }
      <% } -%>
      <% if (sass) { -%>,
        { test: /\.s[ac]ss$/, use: [ 'style-loader', 'css-loader', 'sass-loader' ] }
      <% } -%>
    ]
  }
}
