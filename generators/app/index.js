const { BaseGenerator } = require('scaffolds-common')
const beautify = require('gulp-beautify')
const gulpif = require('gulp-if')

module.exports = class extends BaseGenerator {
  constructor(args, opts) {
    super(args, opts)
  }

  initializing () {
    this.preventLoadInGeneratorDir()
  }

  async prompting() {
    this.answers = await this.prompt([
      {
        type: 'confirm',
        name: 'html',
        message: '是否生成独立的 html 文件',
        default: false
      },
      {
        type: 'confirm',
        name: 'css',
        message: '是否支持 css 加载器，并生成独立的 css 文件',
        default: false
      },
    ])
    if (this.answers.css) {
      Object.assign(this.answers, await this.prompt({
        type: 'confirm',
        name: 'sass',
        message: '是否使用 sass 预编译器',
        default: true
      }))
    } else {
      this.answers.sass = false
    }
    Object.assign(this.answers, await this.prompt({
      type: 'confirm',
      name: 'static',
      message: '是否支持静态目录',
      default: false
    }))
  }

  writing() {
    this.registerTransformStream(
      gulpif(
        file => /\.(js|json)$/.test(file.path),
        beautify({ indent_size: 2 })
      )
    )

    const templates = {
      '.babelrc': '.babelrc',
      '.gitignore': '.gitignore',
      'README.md': 'README.md',
      'webpack.config.js': 'webpack.config.js',
      'package.json': 'package.json'
    }

    if (this.answers.html) templates['src/index.html'] = 'src/index.html'
    templates['src/index.js'] = 'src/index.js'
    if (this.answers.css) {
      templates['src/index.css'] = this.answers.sass ? 'src/index.scss' : 'src/index.css'
    }

    for (const source in templates) {
      const dest = templates[source]
      this.fs.copyTpl(
        this.templatePath(source),
        this.destinationPath(dest),
        this.answers, // context
        {}, // templateOptions 
        {}
      )
    }

    this._extendPackageJson()
  }

  _extendPackageJson() {
    const pkgJson = {
      devDependencies: {
	'@babel/core': '^7.8.3',
	'@babel/preset-env': '^7.8.3',
	'babel-loader': '^8.0.6',
	'clean-webpack-plugin': '^3.0.0',
	'html-webpack-plugin': '^3.2.0',
	'webpack': '^4.41.5',
	'webpack-cli': '^3.3.10',
	'webpack-dev-server': '^3.10.1'
      }
    }

    if (this.answers.static) {
      Object.assign(pkgJson.devDependencies, {
        "copy-webpack-plugin": "^6.4.1"
      })
    }

    if (this.answers.css) {
      Object.assign(pkgJson.devDependencies, {
	'css-loader': '^3.4.2',
	'style-loader': '^1.1.3'
      })
      if (this.answers.sass) {
        Object.assign(pkgJson.devDependencies, {
          'sass': '^1.25.0',
          'sass-loader': '^8.0.2'
        })
      }
    }

    pkgJson.devDependencies = this._sortedObjectByKeys(pkgJson.devDependencies)
    this.fs.extendJSON(this.destinationPath('package.json'), pkgJson)
  }

  _sortedObjectByKeys(unordered) {
    const ordered = {}
    Object.keys(unordered).sort().forEach(function(key) {
      ordered[key] = unordered[key];
    })
    return ordered 
  }
}
