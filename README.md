# generator-webpack-webapp

> 一个基于 WebPack 构建的 Web 应用生成器，支持 Babel、Sass 等

- 支持 Babel
- 支持 Sass
- 支持 js 代码热重载
- 支持生成独立的 HTML、CSS 文件，且支持热重载

## 如何使用

首先，注册生成器：

    cd path/to/generators/webpack-webapp
    yarn
    yarn link scaffolds-common
    yarn link

然后，生成脚手架：

    mkdir sample-app
    cd sample-app
    yo webpack-webapp

# License

MIT
